FROM prom/alertmanager:latest

COPY ./alertmanager/alertmanager.yml /etc/alertmanager/alertmanager.yml

CMD [ "--config.file=/etc/alertmanager/alertmanager.yml" ]